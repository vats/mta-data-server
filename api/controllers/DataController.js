/**
 * DataController
 *
 * @description :: Server-side logic for managing data
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var dummyjson = require('dummy-json');

module.exports = {

	status: function(req, res, next){
		var template = '{\
		    "processed": {{int 0 100}},\
		    "attempts": {{int 0 100}},\
		    "deliveries": {{int 0 100}},\
		    "failures": {{int 0 100}},\
		    "nottried": {{int 0 100}},\
		    "discarded": {{int 0 100}}\
		  }';
		var result = dummyjson.parse(template); // Returns a string
		var obj = JSON.parse(result);
		return res.json(obj);
	},

	summary: function(req, res, next){
		var template =  '[\
		{{#repeat 20}}\
		{\
          "attempts"      :   [{{int 0 100}}, {{int 0 20}}],\
          "deliveries"    :   [{{int 0 100}}, {{int 0 20}}],\
          "failures"      :   [{{int 0 50}}, {{int 0 20}}],\
          "processed"     :   [{{int 0 1000}}, {{int 0 20}}],\
          "nottried"      :  [{{int 0 100}}, {{int 0 20}}],\
          "discarded"     :   [{{int 0 100}}, {{int 0 20}}]\
      }\
			{{/repeat}}\
		]';
			var result = dummyjson.parse(template); // Returns a string
			return res.json({data:result});
	}

};
